# intel_hex_lib

This project is an opensource rust library of useful functions for interacting with Intel Hex records. A link to the specification can be found at [Wikipedia](https://en.wikipedia.org/wiki/Intel_HEX).

## Building
CI/CD is running, but caching is not possible.


## Running Tests
```cargo tarpaulin -v --out Html```


## Documentation
Documentation can be found [here](https://robert.ernst.paf.gitlab.io/intel-hex-lib/intel_hex_lib/)
