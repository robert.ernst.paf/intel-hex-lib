use core::fmt;
use std::fmt::Write;
use std::str;

use crate::checksum::checksum;
use crate::error::IntelHexLibError;

mod char_counts {
    /// The smallest record (excluding start code) is Byte Count + Address + Record Type + Checksum.
    pub const SMALLEST_RECORD_EXCLUDING_START_CODE: usize = (1 + 2 + 1 + 1) * 2;
    /// The smallest record (excluding start code) {Smallest} + a 255 byte payload region.
    pub const LARGEST_RECORD_EXCLUDING_START_CODE: usize = (1 + 2 + 1 + 255 + 1) * 2;
}

mod payload_sizes {
    /// An EoF record has no payload.
    pub const END_OF_FILE: usize = 0;
    /// An Extended Segment Address has a 16-bit payload.
    pub const EXTENDED_SEGMENT_ADDRESS: usize = 2;
    /// An Start Segment Address has two 16-bit payloads.
    pub const START_SEGMENT_ADDRESS: usize = 4;
    /// An Extended Linear Address has a 16-bit payload.
    pub const EXTENDED_LINEAR_ADDRESS: usize = 2;
    /// An Start Linear Address has a 32-bit payload.
    pub const START_LINEAR_ADDRESS: usize = 4;
}

/// This data structure holds a single line from a Intel Hex File.
///
/// [wikipedia](https://en.wikipedia.org/wiki/Intel_HEX)
#[derive(PartialEq, Eq, Debug, Clone, Hash)]
pub enum Record {
    /// Specifies a 16-bit offset address and up to 255 bytes of data.
    /// Availability: I8HEX, I16HEX and I32HEX.
    Data {
        /// The offset of the data record in memory.
        offset: u16,
        /// Up to 255 bytes of data to be written to memory.
        value: Vec<u8>,
    },

    /// Indicates the end of the object file. Must occur exactly once per file, at the end.
    /// Availability: I8HEX, I16HEX and I32HEX.
    EndOfFile,

    /// Specifies bits 4-19 of the Segment Base Address (SBA) to address up to 1MiB.
    /// Availability: I16HEX.
    ExtendedSegmentAddress(u16),

    /// Specifies the 20-bit segment address.
    /// Availability: I16HEX.
    StartSegmentAddress(u32),

    /// Specifies the upper 16 bits of a 32-bit linear address.
    /// The lower 16 bits are derived from the Data record load offset.
    /// Availability: I32HEX.
    ExtendedLinearAddress(u16),

    /// Specifies the execution start address for the object file.
    /// This is the 32-bit linear address for register EIP.
    /// Availability: I32HEX.
    StartLinearAddress(u32),
}

impl TryFrom<&str> for Record {
    type Error = IntelHexLibError;

    fn try_from(line_in: &str) -> Result<Self, Self::Error> {
        Record::from_record_string(line_in)
    }
}

impl Record {
    ///
    /// Constructs a new `Record` by parsing `string`.
    ///
    /// # Examples
    ///
    /// ```rust
    /// use intel_hex_lib::Record;
    ///
    /// let record = Record::from_record_string(":00000001FF").unwrap();
    /// ```
    ///
    pub fn from_record_string(string: &str) -> Result<Self, IntelHexLibError> {
        let string = string.trim();
        if let Some(':') = string.chars().next() {
        } else {
            return Err(IntelHexLibError::MissingStartCode);
        }

        let data_portion = &string[1..];
        let data_portion_length = data_portion.chars().count();

        // Validate all characters are hexadecimal before checking the digit counts for more accurate errors.
        if !data_portion
            .chars()
            .all(|character| character.is_ascii_hexdigit())
        {
            return Err(IntelHexLibError::ContainsInvalidCharacters);
        }

        // Basic sanity-checking the input record string.
        if data_portion_length < char_counts::SMALLEST_RECORD_EXCLUDING_START_CODE {
            return Err(IntelHexLibError::RecordTooShort);
        } else if data_portion_length > char_counts::LARGEST_RECORD_EXCLUDING_START_CODE {
            return Err(IntelHexLibError::RecordTooLong);
        } else if (data_portion_length % 2) != 0 {
            return Err(IntelHexLibError::RecordNotEvenLength);
        }

        // Convert the character stream to bytes.
        let mut data_bytes = data_portion
            .as_bytes()
            .chunks(2)
            .map(|chunk| str::from_utf8(chunk).unwrap())
            .map(|byte_str| u8::from_str_radix(byte_str, 16).unwrap())
            .collect::<Vec<u8>>();

        // Compute the checksum.
        let expected_checksum = data_bytes.pop().unwrap();
        let validated_region_bytes = data_bytes.as_slice();
        let checksum = checksum(validated_region_bytes);

        // The read is failed if the checksum does not match.
        if checksum != expected_checksum {
            return Err(IntelHexLibError::ChecksumMismatch(
                checksum,
                expected_checksum,
            ));
        }

        // Decode header values.
        let length = validated_region_bytes[0];
        let address = u16::from_be_bytes([validated_region_bytes[1], validated_region_bytes[2]]);
        let record_type = validated_region_bytes[3];
        let payload_bytes = &validated_region_bytes[4..];

        // Validate the length of the record matches what was specified in the header.
        if payload_bytes.len() != (length as usize) {
            return Err(IntelHexLibError::PayloadLengthMismatch);
        }

        match record_type {
            types::DATA => {
                // A Data record consists of an address and payload bytes.
                Ok(Record::Data {
                    offset: address,
                    value: Vec::from(payload_bytes),
                })
            }

            types::END_OF_FILE => {
                // An EoF record has no payload.
                match payload_bytes.len() {
                    payload_sizes::END_OF_FILE => Ok(Record::EndOfFile),

                    _ => Err(IntelHexLibError::InvalidLengthForType),
                }
            }

            types::EXTENDED_SEGMENT_ADDRESS => {
                match payload_bytes.len() {
                    payload_sizes::EXTENDED_SEGMENT_ADDRESS => {
                        // The 16-bit extended segment address is encoded big-endian.
                        let address: u16 = u16::from_be_bytes(payload_bytes.try_into().unwrap());
                        Ok(Record::ExtendedSegmentAddress(address))
                    }
                    _ => Err(IntelHexLibError::InvalidLengthForType),
                }
            }

            types::START_SEGMENT_ADDRESS => match payload_bytes.len() {
                payload_sizes::START_SEGMENT_ADDRESS => {
                    let address: u32 = u32::from_be_bytes(payload_bytes.try_into().unwrap());
                    Ok(Record::StartSegmentAddress(address))
                }
                _ => Err(IntelHexLibError::InvalidLengthForType),
            },

            types::EXTENDED_LINEAR_ADDRESS => {
                match payload_bytes.len() {
                    payload_sizes::EXTENDED_LINEAR_ADDRESS => {
                        // The upper 16 bits of the linear address are encoded as a 16-bit big-endian integer.
                        let ela: u16 = u16::from_be_bytes(payload_bytes.try_into().unwrap());
                        Ok(Record::ExtendedLinearAddress(ela))
                    }
                    _ => Err(IntelHexLibError::InvalidLengthForType),
                }
            }

            types::START_LINEAR_ADDRESS => {
                match payload_bytes.len() {
                    payload_sizes::START_LINEAR_ADDRESS => {
                        // The 32-bit value loaded into EIP is encoded as a 32-bit big-endian integer.
                        let sla: u32 = u32::from_be_bytes(payload_bytes.try_into().unwrap());
                        Ok(Record::StartLinearAddress(sla))
                    }

                    _ => Err(IntelHexLibError::InvalidLengthForType),
                }
            }

            _ => Err(IntelHexLibError::UnsupportedRecordType(record_type)),
        }
    }

    ///
    /// Returns the IHEX record representation of the receiver, or an error on failure.
    ///
    pub fn to_record_string(&self) -> Result<String, IntelHexLibError> {
        match self {
            Record::Data { offset, value } => format_record(self.record_type(), *offset, value),

            Record::EndOfFile => format_record(self.record_type(), 0x0000, []),

            Record::ExtendedSegmentAddress(segment_address) => {
                format_record(self.record_type(), 0x0000, segment_address.to_be_bytes())
            }

            Record::StartSegmentAddress(address) => {
                format_record(self.record_type(), 0x0000, address.to_le_bytes())
            }

            Record::ExtendedLinearAddress(linear_address) => {
                format_record(self.record_type(), 0x0000, linear_address.to_le_bytes())
            }

            Record::StartLinearAddress(address) => {
                format_record(self.record_type(), 0x0000, address.to_le_bytes())
            }
        }
    }

    ///
    /// The record type specifier corresponding to the receiver.
    ///
    pub fn record_type(&self) -> u8 {
        match self {
            Record::Data { .. } => types::DATA,
            Record::EndOfFile => types::END_OF_FILE,
            Record::ExtendedSegmentAddress(..) => types::EXTENDED_SEGMENT_ADDRESS,
            Record::StartSegmentAddress { .. } => types::START_SEGMENT_ADDRESS,
            Record::ExtendedLinearAddress(..) => types::EXTENDED_LINEAR_ADDRESS,
            Record::StartLinearAddress(..) => types::START_LINEAR_ADDRESS,
        }
    }
}

impl str::FromStr for Record {
    type Err = IntelHexLibError;
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        Record::from_record_string(input)
    }
}

/// The Record types as specified by the specification.
///
/// [wikipedia](https://en.wikipedia.org/wiki/Intel_HEX)
pub mod types {
    /// Type specifier for a Data record.
    pub const DATA: u8 = 0x00;
    /// Type specifier for an End-Of-File record.
    pub const END_OF_FILE: u8 = 0x01;
    /// Type specifier for an Extended Segment Address record.
    pub const EXTENDED_SEGMENT_ADDRESS: u8 = 0x02;
    /// Type specifier for a Start Segment Address record.
    pub const START_SEGMENT_ADDRESS: u8 = 0x03;
    /// Type specifier for an Extended Linear Address record.
    pub const EXTENDED_LINEAR_ADDRESS: u8 = 0x04;
    /// Type specifier for a Start Linear Address record.
    pub const START_LINEAR_ADDRESS: u8 = 0x05;
}

impl fmt::Display for Record {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Record::Data { offset, value } => {
                write!(f, "Data Record: Offset:{}, value: {:X?}", offset, value)
            }
            Record::EndOfFile => {
                write!(f, "EndOfFile Record")
            }
            Record::ExtendedSegmentAddress(address) => {
                write!(f, "Extended Segment Address Record: Address: {}", address)
            }
            Record::StartSegmentAddress(address) => {
                write!(f, "Start Segment Address Record: Address: {}", address)
            }
            Record::ExtendedLinearAddress(address) => {
                write!(f, "Extended Linear Address Record: Address: {}", address)
            }
            Record::StartLinearAddress(address) => {
                write!(f, "Start Linear Address Record: Address: {}", address)
            }
        }
    }
}

///
/// IHEX records all contain the following fields:
/// `+-----+------------+--------------+----------+------------+-------------+`
/// `| ':' | Length: u8 | Address: u16 | Type: u8 | Data: [u8] | Checkum: u8 |`
/// `+-----+------------+--------------+----------+------------+-------------+`
/// Any multi-byte values are represented big endian.
/// Note that this method will fail if a data record is more than 255 bytes long.
/// This method returns a formatted IHEX record on success with the specified
/// `record_type`, `address` and `data` values. On failure, an error is returned.
///
fn format_record<T>(record_type: u8, address: u16, input: T) -> Result<String, IntelHexLibError>
where
    T: AsRef<[u8]>,
{
    let data = input.as_ref();
    if data.len() > 0xFF {
        return Err(IntelHexLibError::DataExceedsMaximumLength(data.len()));
    }

    // Allocate space for the data region (everything but the start code).
    let data_length = 1 + 2 + 1 + data.len() + 1;
    let mut data_region = Vec::<u8>::with_capacity(data_length);

    // Build the record (excluding start code) up to the checksum.
    data_region.push(data.len() as u8);
    data_region.push(((address & 0xFF00) >> 8) as u8);
    data_region.push((address & 0x00FF) as u8);
    data_region.push(record_type);
    data_region.extend_from_slice(data);

    // Compute the checksum of the data region thus far and append it.
    let checksum = checksum(data_region.as_slice());
    data_region.push(checksum);

    // The result string is twice as long as the record plus the start code.
    let result_length = 1 + (2 * data_length);
    let mut result = String::with_capacity(result_length);

    // Construct the record.
    result.push(':');
    data_region.iter().try_fold(result, |mut acc, byte| {
        write!(&mut acc, "{:02X}", byte)
            .map_err(|_| IntelHexLibError::SynthesisFailed)
            .map(|_| acc)
    })
}
