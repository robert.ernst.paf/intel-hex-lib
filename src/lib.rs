#![warn(missing_docs)]

//! Parsing Intel-Hex-Files
//!
//! This Project is an opensource library of useful functions for
//! interacting with Intel Hex records. A link to the specification can be found at [Wikipedia](https://en.wikipedia.org/wiki/Intel_HEX)
//!
//! The project was inspired by [Intel Hex Lib](https://gitlab.com/bwheel1990/intelhexlib) from Byron Wheeler

//use core::slice::SlicePattern;
use std::{
    cmp::min,
    collections::{btree_map::Range, BTreeMap},
    fs::{metadata, File},
    io::{BufRead, BufReader, BufWriter, Read, Write},
    ops::RangeBounds,
    path::{Path, PathBuf},
};

use error::IntelHexLibError;
pub use record::Record;
mod checksum;
mod error;
mod record;

/// End of line Style
pub enum EOLStyle {
    /// Operating system standard
    Native,
    /// CR and LF as end of line
    CRLF,
}

const DEFAULT_PADDING: u8 = 0xFF;

/// This library contains functions for decoding Intel Hex into
/// [Record]s and Encoding [Record]s into Inte Hex.
#[derive(Default)]
pub struct IntelHex {
    datas: BTreeMap<u32, u8>,
    start_segment_address: Option<u32>,
    start_linear_address: Option<u32>,
    padding: u8,
}

impl IntelHex {
    /// Creates an [`IntelHex`] object. The object will be empty.
    pub fn new() -> Self {
        Self {
            datas: BTreeMap::new(),
            start_segment_address: None,
            start_linear_address: None,
            padding: DEFAULT_PADDING,
        }
    }

    fn get_1byte_array(&self, address: u32) -> [u8; 1] {
        const BYTESIZE: usize = 1;
        let array: [u8; BYTESIZE] = [*self.datas.get(&address).unwrap_or(&self.padding)];
        array
    }

    fn get_2byte_array(&self, address: u32) -> [u8; 2] {
        const BYTESIZE: usize = 2;
        let array: [u8; BYTESIZE] = [
            self.get_1byte_array(address),
            self.get_1byte_array(address + 1),
        ]
        .concat()
        .try_into()
        .unwrap();
        array
    }

    fn get_4byte_array(&self, address: u32) -> [u8; 4] {
        const BYTESIZE: usize = 4;
        let array: [u8; BYTESIZE] = [
            self.get_2byte_array(address),
            self.get_2byte_array(address + 2),
        ]
        .concat()
        .try_into()
        .unwrap();
        array
    }

    fn get_8byte_array(&self, address: u32) -> [u8; 8] {
        const BYTESIZE: usize = 8;
        let array: [u8; BYTESIZE] = [
            self.get_4byte_array(address),
            self.get_4byte_array(address + 4),
        ]
        .concat()
        .try_into()
        .unwrap();
        array
    }

    /// This function returns the [u8] value from the referenced address
    ///
    /// # Parameters:
    /// - address: Address of the desired value
    ///
    /// # Return value
    ///
    /// Value of the pointer to address
    pub fn get_u8_val(&self, address: u32) -> u8 {
        u8::from_le_bytes(self.get_1byte_array(address))
    }

    /// This function returns the [u16] value from the referenced address
    ///
    /// # Parameters:
    /// - address: Address of the desired value
    ///
    /// # Return value
    ///
    /// Value of the pointer to address
    pub fn get_u16_val(&self, address: u32) -> u16 {
        u16::from_le_bytes(self.get_2byte_array(address))
    }

    /// This function returns the [u32] value from the referenced address
    ///
    /// # Parameters:
    /// - address: Address of the desired value
    ///
    /// # Return value
    ///
    /// Value of the pointer to address
    pub fn get_u32_val(&self, address: u32) -> u32 {
        u32::from_le_bytes(self.get_4byte_array(address))
    }

    /// This function returns the [u64] value from the referenced address
    ///
    /// # Parameters:
    /// - address: Address of the desired value
    ///
    /// # Return value
    ///
    /// Value of the pointer to address
    pub fn get_u64_val(&self, address: u32) -> u64 {
        u64::from_le_bytes(self.get_8byte_array(address))
    }

    /// This function returns the [i8] value from the referenced address
    ///
    /// # Parameters:
    /// - address: Address of the desired value
    ///
    /// # Return value
    ///
    /// Value of the pointer to address
    pub fn get_i8_val(&self, address: u32) -> i8 {
        i8::from_le_bytes(self.get_1byte_array(address))
    }

    /// This function returns the [i16] value from the referenced address
    ///
    /// # Parameters:
    /// - address: Address of the desired value
    ///
    /// # Return value
    ///
    /// Value of the pointer to address
    pub fn get_i16_val(&self, address: u32) -> i16 {
        i16::from_le_bytes(self.get_2byte_array(address))
    }

    /// This function returns the [i32] value from the referenced address
    ///
    /// # Parameters:
    /// - address: Address of the desired value
    ///
    /// # Return value
    ///
    /// Value of the pointer to address
    pub fn get_i32_val(&self, address: u32) -> i32 {
        i32::from_le_bytes(self.get_4byte_array(address))
    }

    /// This function returns the [i64] value from the referenced address
    ///
    /// # Parameters:
    /// - address: Address of the desired value
    ///
    /// # Return value
    ///
    /// Value of the pointer to address
    pub fn get_i64_val(&self, address: u32) -> i64 {
        i64::from_le_bytes(self.get_8byte_array(address))
    }

    /// This function returns the [f32] value from the referenced address
    ///
    /// # Parameters:
    /// - address: Address of the desired value
    ///
    /// # Return value
    ///
    /// Value of the pointer to address
    pub fn get_f32_val(&self, address: u32) -> f32 {
        f32::from_le_bytes(self.get_4byte_array(address))
    }

    /// This function returns the [f64] value from the referenced address
    ///
    /// # Parameters:
    /// - address: Address of the desired value
    ///
    /// # Return value
    ///
    /// Value of the pointer to address
    pub fn get_f64_val(&self, address: u32) -> f64 {
        f64::from_le_bytes(self.get_8byte_array(address))
    }

    /// This function returns the [Vec<u8>] array value from the referenced address
    /// with the size
    ///
    /// # Parameters:
    /// - address: Address of the desired value
    /// - size: length of the array
    ///
    /// # Return value
    ///
    /// Value of the pointer to address
    pub fn get_u8_val_vec(&self, address: u32, size: usize) -> Vec<u8> {
        const BYTESIZE: usize = 1;
        let mut vec: Vec<u8> = Vec::with_capacity(size);
        for i in 0..size {
            vec.push(self.get_u8_val(address + u32::try_from(BYTESIZE * i).unwrap()));
        }
        vec
    }

    /// This function returns the [Vec<u16>] array value from the referenced address
    /// with the size
    ///
    /// # Parameters:
    /// - address: Address of the desired value
    /// - size: length of the array
    ///
    /// # Return value
    ///
    /// Value of the pointer to address
    pub fn get_u16_val_vec(&self, address: u32, size: usize) -> Vec<u16> {
        const BYTESIZE: usize = 2;
        let mut vec: Vec<u16> = Vec::with_capacity(size);
        for i in 0..size {
            vec.push(self.get_u16_val(address + u32::try_from(BYTESIZE * i).unwrap()));
        }
        vec
    }

    /// This function returns the [Vec<u32>] array value from the referenced address
    /// with the size
    ///
    /// # Parameters:
    /// - address: Address of the desired value
    /// - size: length of the array
    ///
    /// # Return value
    ///
    /// Value of the pointer to address
    pub fn get_u32_val_vec(&self, address: u32, size: usize) -> Vec<u32> {
        const BYTESIZE: usize = 4;
        let mut vec: Vec<u32> = Vec::with_capacity(size);
        for i in 0..size {
            vec.push(self.get_u32_val(address + u32::try_from(BYTESIZE * i).unwrap()));
        }
        vec
    }

    /// This function returns the [Vec<u64>] array value from the referenced address
    /// with the size
    ///
    /// # Parameters:
    /// - address: Address of the desired value
    /// - size: length of the array
    ///
    /// # Return value
    ///
    /// Value of the pointer to address
    pub fn get_u64_val_vec(&self, address: u32, size: usize) -> Vec<u64> {
        const BYTESIZE: usize = 8;
        let mut vec: Vec<u64> = Vec::with_capacity(size);
        for i in 0..size {
            vec.push(self.get_u64_val(address + u32::try_from(BYTESIZE * i).unwrap()));
        }
        vec
    }

    /// This function returns the [Vec<i8>] array value from the referenced address
    /// with the size
    ///
    /// # Parameters:
    /// - address: Address of the desired value
    /// - size: length of the array
    ///
    /// # Return value
    ///
    /// Value of the pointer to address
    pub fn get_i8_val_vec(&self, address: u32, size: usize) -> Vec<i8> {
        const BYTESIZE: usize = 1;
        let mut vec: Vec<i8> = Vec::with_capacity(size);
        for i in 0..size {
            vec.push(self.get_i8_val(address + u32::try_from(BYTESIZE * i).unwrap()));
        }
        vec
    }

    /// This function returns the [Vec<i16>] array value from the referenced address
    /// with the size
    ///
    /// # Parameters:
    /// - address: Address of the desired value
    /// - size: length of the array
    ///
    /// # Return value
    ///
    /// Value of the pointer to address
    pub fn get_i16_val_vec(&self, address: u32, size: usize) -> Vec<i16> {
        const BYTESIZE: usize = 2;
        let mut vec: Vec<i16> = Vec::with_capacity(size);
        for i in 0..size {
            vec.push(self.get_i16_val(address + u32::try_from(BYTESIZE * i).unwrap()));
        }
        vec
    }

    /// This function returns the [Vec<i32>] array value from the referenced address
    /// with the size
    ///
    /// # Parameters:
    /// - address: Address of the desired value
    /// - size: length of the array
    ///
    /// # Return value
    ///
    /// Value of the pointer to address
    pub fn get_i32_val_vec(&self, address: u32, size: usize) -> Vec<i32> {
        const BYTESIZE: usize = 4;
        let mut vec: Vec<i32> = Vec::with_capacity(size);
        for i in 0..size {
            vec.push(self.get_i32_val(address + u32::try_from(BYTESIZE * i).unwrap()));
        }
        vec
    }

    /// This function returns the [Vec<i64>] array value from the referenced address
    /// with the size
    ///
    /// # Parameters:
    /// - address: Address of the desired value
    /// - size: length of the array
    ///
    /// # Return value
    ///
    /// Value of the pointer to address
    pub fn get_i64_val_vec(&self, address: u32, size: usize) -> Vec<i64> {
        const BYTESIZE: usize = 8;
        let mut vec: Vec<i64> = Vec::with_capacity(size);
        for i in 0..size {
            vec.push(self.get_i64_val(address + u32::try_from(BYTESIZE * i).unwrap()));
        }
        vec
    }

    /// This function returns the [Vec<f32>] array value from the referenced address
    /// with the size
    ///
    /// # Parameters:
    /// - address: Address of the desired value
    /// - size: length of the array
    ///
    /// # Return value
    ///
    /// Value of the pointer to address
    pub fn get_f32_val_vec(&self, address: u32, size: usize) -> Vec<f32> {
        const BYTESIZE: usize = 4;
        let mut vec: Vec<f32> = Vec::with_capacity(size);
        for i in 0..size {
            vec.push(self.get_f32_val(address + u32::try_from(BYTESIZE * i).unwrap()));
        }
        vec
    }

    /// This function returns the [Vec<f64>] array value from the referenced address
    /// with the size
    ///
    /// # Parameters:
    /// - address: Address of the desired value
    /// - size: length of the array
    ///
    /// # Return value
    ///
    /// Value of the pointer to address
    pub fn get_f64_val_vec(&self, address: u32, size: usize) -> Vec<f64> {
        const BYTESIZE: usize = 8;
        let mut vec: Vec<f64> = Vec::with_capacity(size);
        for i in 0..size {
            vec.push(self.get_f64_val(address + u32::try_from(BYTESIZE * i).unwrap()));
        }
        vec
    }

    /// Returens the start segment address if present in hex file,
    /// otherwise None
    pub fn start_segment_address(&self) -> Option<u32> {
        self.start_segment_address
    }

    /// Returens the start linear address if present in hex file,
    /// otherwise None
    pub fn start_linear_address(&self) -> Option<u32> {
        self.start_linear_address
    }

    fn get_eol_textfile(eolstyle: EOLStyle) -> &'static str {
        match eolstyle {
            EOLStyle::Native => "\n",
            EOLStyle::CRLF => {
                if cfg!(windows) {
                    "\r\n"
                } else {
                    "\n"
                }
            }
        }
    }

    /// Write data to file f in HEX format.
    ///
    /// # Arguments
    /// `filename`: filename or file-like object for writing
    /// - `write_start_address`: enable or disable writing start address
    ///                       record to file (enabled by default).
    ///                       If there is no start address in obj, nothing
    ///                       will be written regardless of this setting.
    /// - `eolstyle`: can be used to force CRLF line-endings
    ///               for output file on different platforms.
    ///               Supported eol styles: `EOLStyle::Mative`, `EOLStyle::CRLF`.
    /// - `byte_count`: number of bytes in the data field
    pub fn write_hex_file<P>(
        &self,
        filename: P,
        write_start_address: bool,
        eolstyle: Option<EOLStyle>,
        byte_count: Option<u32>,
    ) -> Result<(), IntelHexLibError>
    where
        P: AsRef<Path>,
    {
        let eolstyle = eolstyle.unwrap_or(EOLStyle::Native);
        let byte_count = byte_count.unwrap_or(16);
        if !(1..=255).contains(&byte_count) {
            return Err(IntelHexLibError::WrongByteCount(byte_count));
        }
        let file = File::open(filename).expect("File could not be opend");
        let mut writer = BufWriter::new(file);
        let eol = Self::get_eol_textfile(eolstyle);

        // Write start address if selected
        if write_start_address {
            if let Some(start_address) = self.start_segment_address {
                let record = Record::StartSegmentAddress(start_address);
                match record.to_record_string() {
                    Ok(string) => write!(writer, "{}{}", string, eol).expect("Writer Error"),
                    Err(err) => return Err(err),
                };
            } else if let Some(start_address) = self.start_linear_address {
                let record = Record::StartLinearAddress(start_address);
                match record.to_record_string() {
                    Ok(string) => write!(writer, "{}{}", string, eol).expect("Writer error"),
                    Err(err) => return Err(err),
                };
            } else {
                return Err(IntelHexLibError::InvalidStartAddress);
            }
        }

        // Write data
        let need_offset_record = self.maxaddr().unwrap_or(0) > 65535;
        let mut high_ofs = 0;
        let mut cur_addr = self.minaddr().unwrap();
        while cur_addr <= self.maxaddr().unwrap() {
            if need_offset_record {
                high_ofs = cur_addr >> 16;
                let record = Record::ExtendedLinearAddress(high_ofs.try_into().unwrap());
                match record.to_record_string() {
                    Ok(string) => write!(writer, "{}{}", string, eol).expect("Writer Error"),
                    Err(err) => return Err(err),
                };
            }
            loop {
                // produce one record
                let low_addr = (cur_addr & 0xFFFF_u32) as u16;
                // chain_len off by 1
                let mut chain_len = min(
                    min(byte_count - 1, 65535 - low_addr as u32),
                    self.maxaddr().unwrap() - cur_addr,
                );

                // search continious chain
                let stop_addr = cur_addr + chain_len;
                let mut values = Vec::new();
                for (index, address) in (cur_addr..stop_addr).enumerate() {
                    match self.datas.get(&address) {
                        Some(value) => values.push(*value),
                        None => {
                            chain_len = index as u32;
                            break;
                        }
                    };
                }
                let record = Record::Data {
                    offset: low_addr,
                    value: values,
                };
                match record.to_record_string() {
                    Ok(string) => {
                        write!(writer, "{}{}", string, eol).expect("Write Error");
                    }
                    Err(err) => {
                        return Err(err);
                    }
                }
                match self.datas.range((cur_addr + chain_len)..).next() {
                    Some((new_address, _)) => {
                        cur_addr = *new_address;
                    }
                    None => {
                        cur_addr = self.maxaddr().unwrap() + 1;
                        break;
                    }
                }
                let high_addr = cur_addr >> 16;
                if high_addr > high_ofs {
                    break;
                }
            }
        }
        Ok(())
    }

    /// Load hex file into internal buffer. This is not necessary
    /// if object was initialized with source set. This will overwrite
    /// addresses if object was already initialized.
    ///
    /// # Arguments
    /// - `filename`: file name or file-like object
    pub fn from_hex_file<P>(&mut self, filename: P) -> Result<(), IntelHexLibError>
    where
        P: AsRef<Path>,
    {
        let file = File::open(&filename).expect("no file found");
        let lines = BufReader::new(file).lines();
        let mut records: Vec<Option<Record>> = Vec::new();
        let mut extended_linear_address: u16 = 0;
        let mut extended_segment_address: u16 = 0;
        for line in lines.flatten() {
            let record = Record::from_record_string(line.as_str());
            if let Ok(record) = record {
                match record {
                    Record::Data { offset, value } => {
                        let mut address: u32 = u32::from(extended_linear_address) << 16;
                        address = address + extended_segment_address as u32 * 16 + offset as u32;
                        for datavalue in value {
                            if self.datas.contains_key(&address) {
                                return Err(IntelHexLibError::AddressOverlapError(address));
                            }
                            self.datas.insert(address, datavalue);
                            address += 1;
                        }
                    }
                    Record::EndOfFile => {
                        records.push(Some(record));
                        break;
                    }
                    Record::ExtendedSegmentAddress(address) => {
                        extended_segment_address = address;
                    }
                    Record::StartSegmentAddress(address) => {
                        self.start_segment_address = Some(address);
                    }
                    Record::ExtendedLinearAddress(address) => {
                        extended_linear_address = address;
                        extended_segment_address = 0;
                    }
                    Record::StartLinearAddress(address) => {
                        if self.start_linear_address.is_some()
                            || self.start_segment_address.is_some()
                        {
                            return Err(IntelHexLibError::DuplicateStartAddressRecordError);
                        }
                        self.start_linear_address = Some(address);
                    }
                }
            } else {
                return Err(record.err().unwrap());
            }
        }
        Ok(())
    }

    /// Load bin file into internal buffer. Not needed if source set in
    /// constructor. This will overwrite addresses without warning
    /// if object was already initialized.
    ///
    /// # Arguments
    /// - `filename`: file name or file-like object
    /// - `offset`: starting address offset
    pub fn from_bin_file<P>(&mut self, filename: P, offset: Option<u32>)
    where
        P: AsRef<Path>,
    {
        let mut file = File::open(&filename).expect("no file found");
        let metadata = metadata(&filename).expect("unable to read metadata");
        let mut buffer = vec![0; metadata.len() as usize];
        file.read_exact(&mut buffer).expect("buffer overflow");
        let mut addr = offset.unwrap_or(0);
        for val in buffer {
            self.datas.insert(addr, val);
            addr += 1;
        }
    }

    /// Load data from array or list of bytes.
    /// Similar to loadbin() method but works directly with iterable bytes.
    pub fn from_bytes(&mut self, bytes: &[u8], offset: Option<u32>) {
        let mut addr = offset.unwrap_or(0);
        for val in bytes {
            self.datas.insert(addr, *val);
            addr += 1;
        }
    }

    /// Return default values for start and end if they are None.
    /// If this IntelHex object is empty then it's error to
    /// invoke this method with both start and end as None.
    fn get_start_end(
        &self,
        start: Option<u32>,
        end: Option<u32>,
        size: Option<usize>,
    ) -> Result<(u32, u32), IntelHexLibError> {
        let mut start = start;
        let mut end = end;
        if (start, end) == (None, None) && self.datas.is_empty() {
            return Err(IntelHexLibError::EmptyIntelHexError);
        }
        if let Some(size) = size {
            if start.is_some() && end.is_some() {
                return Err(IntelHexLibError::OverSpecifiedStartEnd);
            }
            if (start, end) == (None, None) {
                start = self.minaddr();
            }
            if let Some(start) = start {
                end = Some(start + size as u32 - 1_u32);
            } else {
                if size as u32 > end.unwrap() {
                    return Err(IntelHexLibError::InvalideSize);
                }
                start = Some(end.unwrap() - size as u32 + 1_u32);
            }
        } else {
            if start.is_none() {
                start = self.minaddr();
            }
            if end.is_none() {
                end = self.maxaddr();
            }
            if start.unwrap() > end.unwrap() {
                (start, end) = (end, start);
            }
        }
        Ok((start.unwrap(), end.unwrap()))
    }

    /// Convert this object to binary form as array. If start and end
    /// unspecified, they will be inferred from the data.
    ///
    /// # Arguments
    /// - `start`: start address of output bytes.
    /// - `end`: end address of output bytes (inclusive).
    /// - `size`: size of the block, used with start or end parameter.
    /// # Return value
    /// array of unsigned char data.
    pub fn to_bin_array(
        &self,
        start: Option<u32>,
        end: Option<u32>,
        size: Option<usize>,
    ) -> Result<Vec<u8>, IntelHexLibError> {
        let pad = self.padding;
        let mut bin: Vec<u8> = Vec::new();
        if self.datas.is_empty() && (start.is_none() || end.is_none()) {
            return Ok(bin);
        }
        if size.is_some() && size.unwrap() == 0 {
            return Err(IntelHexLibError::InvalideSize);
        }
        match self.get_start_end(start, end, size) {
            Ok((start, end)) => {
                for i in start..end + 1 {
                    let val = self.datas.get(&i).unwrap_or(&pad);
                    bin.push(*val);
                }
                Ok(bin)
            }
            Err(err) => Err(err),
        }
    }
    /// Returns all used addresses in sorted order.
    /// # Return value
    /// list of occupied data addresses in sorted order.
    pub fn addresses(&self) -> Vec<u32> {
        let keys = self.datas.keys().cloned().collect();
        keys
    }

    /// Get minimal address of HEX content.
    /// # Return value
    /// minimal address or None if no data
    pub fn minaddr(&self) -> Option<u32> {
        return self.datas.keys().min().cloned();
    }

    /// Get maximal address of HEX content.
    /// # Return value
    /// maximal address or None if no data
    pub fn maxaddr(&self) -> Option<u32> {
        return self.datas.keys().max().cloned();
    }

    /// Get requested byte from address.
    /// # Arguments
    /// - `address`: address of byte.
    /// # Return Value
    /// byte if address exists in HEX file, or self.padding
    /// if no data found.
    pub fn get(&self, address: u32) -> u8 {
        self.datas.get(&address).cloned().unwrap_or(self.padding)
    }

    /// Constructs a double-ended iterator over a sub-range of addresses in the hex file.
    /// The simplest way is to use the range syntax min..max, thus range(min..max) will yield
    /// elements from min (inclusive) to max (exclusive).
    /// The range may also be entered as (Bound<T>, Bound<T>),
    /// so for example range((Excluded(4), Included(10))) will yield a left-exclusive,
    /// right-inclusive range from 4 to 10.
    pub fn range<T, R>(&self, range: R) -> Range<'_, u32, u8>
    where
        R: RangeBounds<u32>,
    {
        self.datas.range(range)
    }

    /// Insert byte at address
    ///     /// # Arguments
    /// - `address`: address of byte.
    pub fn insert(&mut self, address: u32, value: u8) {
        self.datas.insert(address, value);
    }

    /// Removes the address, if value is present it returns the value otherwise None
    pub fn remove(&mut self, address: u32) -> Option<u8> {
        self.datas.remove(&address)
    }

    /// Moves all values from other into this structure
    pub fn append(&mut self, other: &mut BTreeMap<u32, u8>) {
        self.datas.append(other);
    }

    /// Returns the number of bytes in hex file
    pub fn len(&self) -> usize {
        self.datas.len()
    }

    /// Returns true if no bytes in hex file
    pub fn is_empty(&self) -> bool {
        self.datas.is_empty()
    }

    /// Get Vec[u8] of bytes from given address. If any entries are blank
    /// from addr through addr+length, a NotEnoughDataError exception will
    /// be raised. Padding is not used.
    pub fn get_vec(&self, addr: u32, length: usize) -> Result<Vec<u8>, IntelHexLibError> {
        let mut out = Vec::with_capacity(length);
        for address in addr..addr + length as u32 {
            match self.datas.get(&address) {
                Some(value) => {
                    out.push(*value);
                }
                None => {
                    return Err(IntelHexLibError::NotEnoughData(address));
                }
            }
        }
        Ok(out)
    }
}

impl From<std::io::Lines<BufReader<File>>> for IntelHex {
    /// This method will decode [std::io::Lines] according to the Intel Hex format
    ///
    /// # Attributes
    /// - lines: The lines to decode
    ///
    /// # Returns
    /// - An [IntelHex] struct containing the information of lines
    fn from(lines: std::io::Lines<BufReader<File>>) -> Self {
        let mut records: Vec<Option<Record>> = Vec::new();
        let mut datas: BTreeMap<u32, u8> = BTreeMap::new();
        let mut extended_linear_address: u16 = 0;
        let mut extended_segment_address: u16 = 0;
        let mut start_segment_address: Option<u32> = None;
        let mut start_linear_address: Option<u32> = None;
        let mut min_address = u32::MAX;
        let mut max_address = u32::MIN;
        for line in lines.flatten() {
            let record = Record::from_record_string(line.as_str());
            if let Ok(record) = record {
                match record {
                    Record::Data { offset, value } => {
                        let mut address: u32 = u32::from(extended_linear_address) << 16;
                        address = address + extended_segment_address as u32 * 16 + offset as u32;
                        if address < min_address {
                            min_address = address;
                        }
                        if (address + value.len() as u32) > max_address {
                            max_address = address + value.len() as u32;
                        }
                        for datavalue in value {
                            datas.insert(address, datavalue);
                            address += 1;
                        }
                    }
                    Record::EndOfFile => {
                        records.push(Some(record));
                        break;
                    }
                    Record::ExtendedSegmentAddress(address) => {
                        extended_segment_address = address;
                    }
                    Record::StartSegmentAddress(address) => {
                        start_segment_address = Some(address);
                    }
                    Record::ExtendedLinearAddress(address) => {
                        extended_linear_address = address;
                        extended_segment_address = 0;
                    }
                    Record::StartLinearAddress(address) => {
                        start_linear_address = Some(address);
                    }
                }
            } else {
                println!("{:?}", record.err().unwrap());
            }
        }
        Self {
            datas,
            start_linear_address,
            start_segment_address,
            padding: 0xFF,
        }
    }
}

impl From<File> for IntelHex {
    /// This method will decode [`std::fs::File`] according to the Intel Hex format
    ///
    /// # Attributes
    /// - file: The file to decode
    ///
    /// # Returns
    /// - An [`IntelHex`] struct containing the information of file
    fn from(file: File) -> Self {
        let reader = BufReader::new(file);
        let lines = reader.lines();
        IntelHex::from(lines)
    }
}

impl From<String> for IntelHex {
    /// This method will decode [`String`] according to the Intel Hex format
    ///
    /// # Attributes
    /// - file_name: The file to decode
    ///
    /// # Returns
    /// - An [`IntelHex`] struct containing the information of file
    fn from(file_name: String) -> Self {
        let file = File::open(file_name).unwrap();
        IntelHex::from(file)
    }
}

impl From<&PathBuf> for IntelHex {
    /// This method will decode [`PathBuf`] according to the Intel Hex format
    ///
    /// # Attributes
    /// - file_name: The file to decode
    ///
    /// # Returns
    /// - An [`IntelHex`] struct containing the information of file
    fn from(file_name: &PathBuf) -> Self {
        if let Ok(file) = File::open(file_name) {
            println!("File successfully opend");
            IntelHex::from(file)
        } else {
            eprintln!("Opening file was not possible!");
            IntelHex::default()
        }
    }
}

#[cfg(test)]
mod tests {
    use std::{fs::File, io::Write};

    use tempfile::tempdir;

    use crate::IntelHex;

    #[test]
    fn it_works() {
        let dir = tempdir().unwrap();
        let file_path = dir.path().join("test.hex");
        println!("File Path: {:?}", file_path);

        let mut f = match File::create(&file_path) {
            Ok(file) => file,
            Err(_) => {
                panic!("Can't create file");
            }
        };
        println!("File was successfully created");
        let res = f.write_all(
            b":1003200080937A0080917A00826080937A00809135
            :100330007A00816080937A0080917A00806880934F
            :100340007A001092C100EDE9F0E02491E9E8F0E0D4
            :100350008491882399F090E0880F991FFC01E85957
            :10036000FF4FA591B491FC01EE58FF4F85919491F8
            :100370008FB7F894EC91E22BEC938FBFC0E0D0E004
            :1003800081E00E9470000E94DE0080E00E94700008
            :100390000E94DE002097A1F30E940000F1CFF894A4
            :0203A000FFCF8D
            :107E0000112484B714BE81FFF0D085E080938100F7
            :107E100082E08093C00088E18093C10086E0809377
            :107E2000C20080E18093C4008EE0C9D0259A86E02C
            :107E300020E33CEF91E0309385002093840096BBD3
            :107E4000B09BFECF1D9AA8958150A9F7CC24DD24C4
            :107E500088248394B5E0AB2EA1E19A2EF3E0BF2EE7
            :107E6000A2D0813461F49FD0082FAFD0023811F036
            :107E7000013811F484E001C083E08DD089C08234E0",
        );
        if res.is_err() {
            panic!("Writing temporary file not possible");
        }
        if f.sync_all().is_err() {
            panic!("Syncing file not possible");
        }
        println!("File was sucessfully written");

        let intel_hex = IntelHex::from(&file_path);
        assert_eq!(intel_hex.len(), 258, "Not all bytes read");
        println!("File was sucessfully parsed");

        if dir.close().is_err() {
            panic!("Cleaning up temp directory not possible");
        }
    }

    #[test]
    fn test_get_values() {
        let dir = tempdir().unwrap();
        let file_path = dir.path().join("test.hex");
        println!("File Path: {:?}", file_path);

        let mut f = match File::create(&file_path) {
            Ok(file) => file,
            Err(_) => {
                panic!("Can't create file");
            }
        };
        println!("File was successfully created");
        let res = f.write_all(
            b":10010000214601360121470136007EFE09D2190140
            :100110002146017E17C20001FF5F16002148011928
            :10012000194E79234623965778239EDA3F01B2CAA7
            :100130003F0156702B5E712B722B732146013421C7
            :00000001FF",
        );
        if res.is_err() {
            panic!("Writing temporary file not possible");
        }
        if f.sync_all().is_err() {
            panic!("Syncing file not possible");
        }
        println!("File was sucessfully written");

        let intel_hex = IntelHex::from(&file_path);
        println!("File was sucessfully parsed");

        assert_eq!(
            intel_hex.get_1byte_array(0x100),
            [0x21],
            "Auslesen von 1 Byte am Anfang"
        );

        assert_eq!(
            intel_hex.get_1byte_array(0x101),
            [0x46],
            "Auslesen von 1 Byte am Anfang"
        );

        assert_eq!(
            intel_hex.get_1byte_array(0x112),
            [0x01],
            "Auslesen von 1 Byte in der Mitte"
        );

        assert_eq!(
            intel_hex.get_1byte_array(0x13F),
            [0x21],
            "Auslesen von 1 Byte am Ende"
        );

        assert_eq!(
            intel_hex.get_2byte_array(0x101),
            [0x46, 0x01],
            "Auslesen von 2 Byte am Anfang"
        );

        assert_eq!(
            intel_hex.get_8byte_array(0x10D),
            [0xD2, 0x19, 0x01, 0x21, 0x46, 0x01, 0x7E, 0x17],
            "Auslesen von 8 Byte in der Mitte mit Record-Umbruch"
        );

        drop(f);

        if dir.close().is_err() {
            panic!("Cleaning up temp directory not possible");
        }
    }
}
