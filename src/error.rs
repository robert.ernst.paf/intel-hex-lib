use std::{error::Error, fmt};

#[derive(PartialEq, Eq, Clone, Copy, Debug, Hash)]
pub enum IntelHexLibError {
    /// The record provided does not begin with a ':'.
    MissingStartCode,
    /// The record provided is shorter than the smallest valid.
    RecordTooShort,
    /// The record provided exceeds the maximum size (255b payload).
    RecordTooLong,
    /// The record is not an even number of bytes.
    RecordNotEvenLength,
    /// The record is not all hexadecimal characters.
    ContainsInvalidCharacters,
    /// The checksum did not match.
    ChecksumMismatch(u8, u8),
    /// The record is not the length it claims.
    PayloadLengthMismatch,
    /// The record type is not supported.
    UnsupportedRecordType(u8),
    /// The payload length does not match the record type.
    InvalidLengthForType,
    /// A record contains data too large to represent.
    DataExceedsMaximumLength(usize),
    /// Object does not end in an EoF record.
    MissingEndOfFileRecord,
    /// Object contains multiple EoF records.
    MultipleEndOfFileRecords(usize),
    /// Unable to synthesize record string.
    SynthesisFailed,
    /// Wrong byte count for export
    WrongByteCount(u32),
    /// Invalid start address
    InvalidStartAddress,
    /// Empty Hex File
    EmptyIntelHexError,
    /// Start, End and size is speziefied for export
    OverSpecifiedStartEnd,
    /// Specified size for export is invalid
    InvalideSize,
    /// Duplicated Start Address Record
    DuplicateStartAddressRecordError,
    /// Address overlap error
    AddressOverlapError(u32),
    /// Writer Error
    WriterError,
    ///
    NotEnoughData(u32),
}

impl Error for IntelHexLibError {}

impl fmt::Display for IntelHexLibError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            IntelHexLibError::MissingStartCode => write!(f, "missing start code ':'"),
            IntelHexLibError::RecordTooShort => write!(f, "too short"),
            IntelHexLibError::RecordTooLong => write!(f, "too long"),
            IntelHexLibError::RecordNotEvenLength => {
                write!(f, "record does not contain a whole number of bytes")
            }
            IntelHexLibError::ContainsInvalidCharacters => {
                write!(f, "invalid characters encountered in record")
            }
            IntelHexLibError::ChecksumMismatch(found, expecting) => write!(
                f,
                "invalid checksum '{:02X}', expecting '{:02X}'",
                found, expecting,
            ),
            IntelHexLibError::PayloadLengthMismatch => {
                write!(f, "payload length does not match record header")
            }
            IntelHexLibError::UnsupportedRecordType(record_type) => {
                write!(f, "unsupported IHEX record type '{:02X}'", record_type)
            }
            IntelHexLibError::InvalidLengthForType => {
                write!(f, "payload length invalid for record type")
            }
            IntelHexLibError::DataExceedsMaximumLength(bytes) => {
                write!(f, "record has {} bytes (max 255)", bytes)
            }
            IntelHexLibError::MissingEndOfFileRecord => {
                write!(f, "object is missing end of file record")
            }
            IntelHexLibError::MultipleEndOfFileRecords(eofs) => {
                write!(f, "object contains {} end of file records", eofs)
            }
            IntelHexLibError::SynthesisFailed => {
                write!(f, "unable to write string representation of record")
            }
            IntelHexLibError::WrongByteCount(byte_count) => {
                write!(f, "byte count for hex file export is wrong. Value is {} and should be in between 1..255", byte_count)
            }
            IntelHexLibError::InvalidStartAddress => {
                write!(f, "start address should be exported but is not available")
            }
            IntelHexLibError::EmptyIntelHexError => {
                write!(
                    f,
                    "requested operation cannot be executed with empty object"
                )
            }
            IntelHexLibError::OverSpecifiedStartEnd => {
                write!(f, "start, End and size is speziefied for export")
            }
            IntelHexLibError::InvalideSize => {
                write!(f, "specified size for export is invalid")
            }
            IntelHexLibError::DuplicateStartAddressRecordError => {
                write!(f, "duplicated start address record")
            }
            IntelHexLibError::AddressOverlapError(address) => {
                write!(f, "hex file has data overlap at address: {:0X}", address)
            }
            IntelHexLibError::WriterError => {
                write!(f, "writer error")
            }
            IntelHexLibError::NotEnoughData(address) => {
                write!(
                    f,
                    "Bad access at {:0X}. No data to read continues bytes",
                    address
                )
            }
        }
    }
}
